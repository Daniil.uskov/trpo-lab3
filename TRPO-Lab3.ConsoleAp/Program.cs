﻿using System;
using TRPO_Lab3.Lib;

namespace TRPO_Lab3.ConsoleAp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите высоту...");
            string Stringh = Console.ReadLine();
            double h = Convert.ToDouble(Stringh);
            Console.WriteLine("Введите нижний радиус...");
            string StringR = Console.ReadLine();
            double R = Convert.ToDouble(StringR);
            Console.WriteLine("Введите верхний радиус...");
            string Stringr = Console.ReadLine();
            double r = Convert.ToDouble(Stringr);
            var V = Formules.Konys(R, r, h);
            Console.WriteLine($"Объем усеченного конуса равен {V}");
        }
    }
}
